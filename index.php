<!DOCTYPE html>
<html>
<head>
	<title>Activity</title>

	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/darkly/bootstrap.css">
</head>
<body>
	<h1 class="text-center"> Zodiac Sign</h1>
	<div class="col-lg-4 offset-lg-4">
			<form action="controllers/reg-proc.php" class="bg-light p-4" method="POST">
				<div class="form-group">
					<label for="firstname">Name</label>
					<input type="text" name="name" class="form-control">		
				</div>
				<div class="form-group">
					<label for="birthMonth">Birth Month</label>
					<input type="text" name="birthMonth" class="form-control">
				</div>
				<div class="form-group">
					<label for="birthDay">Birth day</label>
					<input type="text" name="birthDay" class="form-control">
				</div>
				<div class="text-center">
					<button type="submit" class="btn btn-success">Enter</button>
				</div>
			</form>

</body>
</html>